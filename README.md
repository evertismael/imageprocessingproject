---

## Image Processing Project

**WORD FINDER SOLVER**

* Find and highlight all hidden words in a Word Finder puzzle.
* First split the words list in separate letters and try to match each letter with the content of the puzzle. Make sure your method is robust to the presence of horizontal/vertical lines or the title letters.

---

## Technologies
* Letter segmentation
* Shape matching
---

## References

* Zhang, Yungang, and Changshui Zhang. "A new algorithm for character segmentation of license plate." Intelligent Vehicles Symposium, 2003. Proceedings. IEEE. IEEE, 2003.
* de Campos, Teo, Bodla Rakesh Babu, and Manik Varma. "Character recognition in natural images." (2009).

## AUTHORS:

* Allen Yusuke Kikuchi (allen.yusuke.kikuchi@vub.be)
* Mohamad Hussein Jarmak (mohamadjarmak@gmail.com)
* Evert Ismael Pocoma Copa (evertismael@gmail.com)
* Jiachen Zhou (zhoujiachenbe@gmail.com)
