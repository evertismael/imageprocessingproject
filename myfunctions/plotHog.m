function [] = plotHog(img,features)
    figure;
    subplot(1,2,1);
    imshow(img);
    
    Nblocks = sqrt(size(features,2)/(9*4));
    
    displayHog = [];
    for r=1:Nblocks
        rowHog = [];
        for c=1:Nblocks
            init = Nblocks*(r-1)*9*4 + (c-1)*9*4 + 1;
            f_4 = features(init:init+9*4-1);
            for ic = 1:4
                f = f_4((ic-1)*9+1:ic*9);
                %f = [1 0.7 0.3 0 1 0.5 0 1 0];
                hogOutBlock_1 = [];
                hogOutBlock_2 = [];
                dim = 8;
                cellHog = zeros(dim,dim);
                for cr = 1:dim
                    for cc = 1:dim
                        xp = cc-dim/2;
                        yp = cr-dim/2;
                        angle = atan(yp/xp)*(180/pi);
                        if angle < 0
                            angle = angle+180;
                        end

                        for i_bin = 1:9
                            bin_center = (i_bin-1)*20 + 10;
                            if abs(bin_center - angle)<=10
                                cellHog(cr,cc) = f(i_bin);
                            end
                        end
                    end
                end
                if ic==1 | ic==2
                    hogOutBlock_1 = [hogOutBlock_1,cellHog];
                end
                if ic==3 | ic==4
                    hogOutBlock_2 = [hogOutBlock_2,cellHog];
                end
            end
            rowHog = [rowHog, [hogOutBlock_1;hogOutBlock_2]];
        end
        displayHog = [displayHog;rowHog];
    end
    subplot(1,2,2);
    imshow(displayHog);
    a= 2;
end