function solverInput = solverInputAdapter(words, matrixLabels)
    
    [NbCharY,NbCharX] = size(matrixLabels);
    MaxChar = max(NbCharX,NbCharY);
    for i = 1:length(words)
        Wtemp = words{i};
        W(i,:) = [Wtemp '~'*ones(1,MaxChar-length(Wtemp))];
    end
    solverInput = W;
end