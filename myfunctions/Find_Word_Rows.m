function [rows_image] = Find_Word_Rows(inImage)

    [rows,columns] = size(inImage);
    inImage = imbinarize(inImage); %%convert to binary image with the threshold half of max value
   
    proj_row = sum(inImage,2);%%initinalization of the horizontal projection and vertical projection vector

    
    window_size = 25;
    window_b = (1/window_size)*ones(1,window_size);
    window_a = 1;
    proj_row_fil = filter(window_b,window_a,proj_row);        %low pass filter 
    proj_row_fil = movmean(proj_row_fil/50,25); %another low pass filter 
    
    water = watershed(proj_row_fil);            %calculate the water sheds
    
    water_histo = zeros(max(water)+1,1);        %take the histogram
    for i=1:length(water)
       water_histo(water(i)+1) = water_histo(water(i)+1) + 1;  
    end
%     water_histo(water_histo <= 40) = 0;
    
% extract the word part
    n = length(water_histo)-1;
    cnt = 0;
    stp = 1;
    while stp>0 && n>0
        if water_histo(n) > 40
            cnt = cnt + 1;
            rows_no(cnt) = n-1;
        elseif water_histo(n) < 40 && water_histo(n+1) > 40
            stp = 0;
        end 
        n = n - 1;
    end
    rows_no = flip(rows_no);
    
% find the start and end of each row
    for i=1:length(rows_no)
        for j=2:length(water)-1
            if water(j)==rows_no(i) && water(j-1)~=rows_no(i)
                rows_se(i,1) = j;
            elseif water(j)==rows_no(i) && water(j+1)~=rows_no(i)
                rows_se(i,2) = j;
            end
        end
    end
   
% extract the word part of the image
    Image_word = inImage(rows_se(1):rows_se(end),1:columns);
    proj_row_word = sum(Image_word,2);  %projection of the word part
%     figure
%     plot(proj_row_word)
    
% chosse more precise rows
    rows_se_re = zeros(length(rows_no),2);
    cnt1 = 1;
    for j=2:length(proj_row_word)-1
         if proj_row_word(j)==columns && proj_row_word(j+1)~=columns
            rows_se_re(cnt1,1) = j;
         elseif proj_row_word(j)==columns && proj_row_word(j-1)~=columns
            rows_se_re(cnt1,2) = j;
            cnt1 = cnt1 + 1;
        end
    end
    rows_ave = round(sum(rows_se_re(:,2)-rows_se_re(:,1))/length(rows_no));
    
% split the figure in rows
    for i=1:length(rows_no)
    rows_image(1:rows_ave,1:columns,i) = imresize(Image_word(rows_se_re(i,1):rows_se_re(i,2),1:columns,1), [rows_ave,columns]);
%     figure
%     imshow(rows_image(:,:,i))
    end
   
    

%     for i=1:length(rows_no)

%     end

%     
%     figure
%     plot(proj_row_fil)
    
%     figure;
%     plot(proj_row_fil); hold on
%     plot(water,'.');
%     title('row projection');
%     
%     figure
%     plot(water_histo,'.')

    
    

end