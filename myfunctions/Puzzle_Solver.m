function [Iout,OUT,Found] = Puzzle_Solver(I,P,W,C,borders)

% [SY,SX,~] = size(C);
% SX = SX;
% SY = SY/3;
SX = borders(3);
SY = borders(4);

I = im2double(I);
I = round(I);

C = im2double(C);
[cy,cx] = size(C);
C = imresize(C,SX/cx);
C = round(C);
C = 1-C;

BorderYTop = borders(1)-floor(SY/4);
BorderYLow = borders(2)+floor(SY/2)-floor(SY/4);

% Make Square Matrix
[NbCharY,NbCharX] = size(P);


if NbCharY > NbCharX
    widthOld = NbCharX;
    heightOld = NbCharY;
    offsetY = abs(NbCharY-NbCharX);
    offsetX = 0;
    P = [P 'X'*ones(NbCharY,offsetY)];
    [NbCharY,NbCharX] = size(P);
elseif NbCharX > NbCharY
    widthOld = NbCharX;
    heightOld = NbCharY;
    offsetX = abs(NbCharY-NbCharX);
    offsetY = 0;
    P = [P;'X'*ones(offsetX,NbCharX)];
    [NbCharY,NbCharX] = size(P);
else
    widthOld = NbCharX;
    heightOld = NbCharY;
    offsetX = 0;
    offsetY = 0;
end

[ResY,ResX,~] = size(I);
BorderX = ResX/2 - SX*(NbCharX)/2;
Icrop = I(BorderYTop:BorderYLow,BorderX:ResX-BorderX);
pad = 200;
l1 = length(BorderYTop:BorderYLow);
l2 = length(BorderX:ResX-BorderX);

Icrop = [ones(l1,pad) Icrop ones(l1,pad)];
Icrop = [ones(pad,l2+2*pad); Icrop; ones(pad,l2+2*pad)];
OUT = P;
Iout = I;

Found = 0;

% COMPUTE LENGTH OF WORDS
[NbrWords,MaxChar] = size(W);

Cmap = rand(5*NbrWords,3);
Cmap(1,:) = [0 0 0];
Cmap(2,:) = [1 1 1];

cross = ['abcdefghijklmnopqrstuvwxyz123456789!@#$%^&*()-='];
L = zeros(1,NbrWords);
for i = 1:NbrWords
    for j = 1:MaxChar
        if (W(i,j) ~= '~')
            L(i) = L(i) +1;
        end
    end
end

% DIAGONAL RESHAPING
% LOWER SIDE
offsety = 0;
offsetx = 0;
for word = 1:NbrWords
    Color = 3+round(rand*NbrWords);
    offsety = 0;
    offsetx = 0;
    sh = 2;
    for m = 1:4
        for i = 2:heightOld
            if(NbCharY-i+1 >= min(L))
                for j = 1:NbCharY-i+1
                    DIAG(i,j) = P(i+j-1,j);
                end
                index = strfind(DIAG(i,:),W(word,1:L(word)));
                if index ~= 0
                    fprintf('\nfound %d : %s (%c)',word,W(word,1:L(word)),cross(word));
                    Found = Found + 1;
                    for Ltr = 1:L(word)
                        posy = pad+SY*(i+index+offsetx+Ltr-3)+1;
                        posx = pad+SX*(index+offsety+Ltr-2)+1;
                        Icrop(posy:SY-1 + posy , posx:SX-1 + posx) = ceil(ceil(Color.*C(1+SY*sh:SY+SY*sh,:))) + Icrop(posy:SY-1 + posy , posx:SX-1 + posx);
                        OUT(i+Ltr-2+index,Ltr+index-1) = num2str(cross(word));
                    end
                end
            end
        end
        if(m == 1)
            OUT = fliplr(OUT);
            P = fliplr(P);
            Icrop = fliplr(Icrop);
            offsety = -offsetY/2;
            offsetx = 0;
        elseif(m == 2)
            OUT = flipud(OUT);
            P = flipud(P);
            Icrop = flipud(Icrop);
            offsety = 0;
            offsetx = -offsetX;
        elseif(m == 3)
            OUT = fliplr(OUT);
            P = fliplr(P);
            Icrop = fliplr(Icrop);
            offsety = -offsetY;
            offsetx = -offsetX;
        elseif(m == 4)
            OUT = flipud(OUT);
            P = flipud(P);
            Icrop = flipud(Icrop);
            offsety = -offsetY;
            offsetx = 0;
        end
    end
    % UPPER SIDE
    offsety = 0;
    offsetx = 0;
    sh = 2;
    for m = 1:4
        for i = 1:widthOld
            if(NbCharX-i+1 >= min(L))
                for j = 1:NbCharX-i+1
                    DIAG(i,j) = P(j,i+j-1);
                end
                index = strfind(DIAG(i,:),W(word,1:L(word)));
                if index ~= 0
                    fprintf('\nfound %d : %s (%c)',word,W(word,1:L(word)),cross(word));
                    Found = Found + 1;
                    for Ltr = 1:L(word)
                        posy = pad+SY*(index+offsetx+Ltr-2)+1;
                        posx = pad+SX*(i+index+offsety+Ltr-3)+1;
                        Icrop(posy:SY-1 + posy , posx:SX-1 + posx) = ceil(ceil(Color.*C(1+SY*sh:SY+SY*sh,:))) + Icrop(posy:SY-1 + posy , posx:SX-1 + posx);
                        OUT(Ltr-1+index,i+Ltr-2+index) = num2str(cross(word));
                    end
                end
            end
        end
        if(m == 1)
            OUT = fliplr(OUT);
            P = fliplr(P);
            Icrop = fliplr(Icrop);
            offsety = -offsetY/2;
            offsetx = 0;
        elseif(m == 2)
            OUT = flipud(OUT);
            P = flipud(P);
            Icrop = flipud(Icrop);
            offsety = 0;
            offsetx = -offsetX;
        elseif(m == 3)
            OUT = fliplr(OUT);
            P = fliplr(P);
            Icrop = fliplr(Icrop);
            offsety = 0;
            offsetx = -offsetX;
        elseif(m == 4)
            OUT = flipud(OUT);
            P = flipud(P);
            Icrop = flipud(Icrop);
            offsety = 0;
            offsetx = -offsetX;
        end
    end
    
    % HORIZONTALLY
    offsety = 0;
    offsetx = 0;
    sh = 0;
    for m = 1:2
        for i=1:heightOld
            index = strfind(P(i,:),W(word,1:L(word)));
            if index ~= 0
                fprintf('\nfound %d : %s (%c)',word,W(word,1:L(word)),cross(word));
                Found = Found + 1;
                OUT(i,index:index+L(word)-1) = num2str(cross(word));
                for cur = 1:L(word)
                    posy = pad+SY*(i-1+offsetx)+1;
                    posx = pad+SX*(index+cur -2+offsety)+1;
                    Icrop(posy:SY-1 + posy,posx:SX-1 + posx) = ceil(ceil(Color.*C(1+SY*sh:SY+SY*sh,:))) + Icrop(posy:SY-1 + posy,posx:SX-1 + posx);
                end
            end
        end
        OUT = fliplr(OUT);
        P = fliplr(P);
        Icrop = fliplr(Icrop);
        if m == 1
            offsetx = 0;
            offsety = -offsetY/2;
        else
            offsetx = -offsetX;
            offsety = 0;
        end
    end
    
    % VERTICALLY
    offsety = 0;
    offsetx = 0;
    sh = 1;
    for m = 1:2
        for i=1:widthOld
            index = strfind(P(:,i)',W(word,1:L(word)));
            if index ~= 0
                fprintf('\nfound %d : %s (%c)',word,W(word,1:L(word)),cross(word));
                Found = Found + 1;
                OUT(index:index+L(word)-1,i) = num2str(cross(word));
                for cur = 1:L(word)
                    posx = pad+SX*(i-1+offsety)+1;
                    posy = pad+SY*(index+cur -2+offsetx)+1;
                    Icrop(posy:SY-1 + posy,posx:SX-1 + posx) = ceil(ceil(Color.*C(1+SY*sh:SY+SY*sh,:))) + Icrop(posy:SY-1 + posy,posx:SX-1 + posx);
                end
            end
        end
        OUT = flipud(OUT);
        P = flipud(P);
        Icrop = flipud(Icrop);
        if m == 1
            offsetx = -offsetX;
            offsety = 0;
        else
            offsetx = 0;
            offsety = -offsetY;
        end
    end
end

% OUT
fprintf('\n\nFound %d words',Found);
NbrWords
if(Found == NbrWords)
    fprintf('\nAll Words have been Found!\n');
end

% OUT
Iout(BorderYTop:BorderYLow,BorderX:ResX-BorderX) = Icrop(pad+1:end-pad,pad+1:end-pad);
figure;
% subplot(1,2,1)
% imshow(I+1,Cmap)
% subplot(1,2,2)
imshow(Iout+1,Cmap)

end