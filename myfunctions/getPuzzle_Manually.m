function puzzle  = getPuzzle_Manually(inputName)
    if strcmp(inputName,'input_01')
         puzzle=['DSDOESZIGZAGSWCH'
            'SDROWYEKOBEYALOI'
            'HNKPLATEAUASCOMA'
            'VEEKARATTLETRAPS'
            'PPRSAOCJPRIAMOUS'
            'YETEQMETOUYDLPTA'
            'RDHTTRUFSSMERMAS'
            'AIEAEOLABHVEAATS'
            'IBDMRAERIESNLFII'
            'CRIIEIVGDSUDTOON'
            'IASTNRADMANRSSNA'
            'DCPSEIRALLICNAST'
            'UWUEWYNIMONGIWIE'
            'JGUESRUOCRETAWPL'];


    elseif strcmp(inputName,'input_02')
        puzzle = ['FCURLICUEDLUOCSO'
            'USAGEINGEENMEALS'
            'DDSSNRERVALENCES'
            'DERIEWEOOSPCSISS'
            'LKLIHTHTMHGETJAE'
            'ECDBSTLOSNLIEUEV'
            'DETALUSNIILJWSKE'
            'RDMSFNETHAOUSTVR'
            'EXTENDICCNARFIPE'
            'DORCSRHOAEOOTFHL'
            'JAYSWALYMCJRMYRY'
            'CSOYNSBECKOBIIAO'
            'TEKCAJEULBSTONSG'
            'SSENSUOLAEZHNGEA'];
       
    elseif strcmp(inputName,'input_03')

        puzzle = ['NDELYYTSAPOOTSRE'
            'OAGMOTYTTKYGCHOP'
            'ICNIVIROTAMFIBSU'
            'TSICNSALUARUOMAR'
            'AOKREOVBLRAAERAI'
            'PWCOTMOICTSLKHKN'
            'UCASIIGSEUOREYDA'
            'CSRCANCZODOMHTTR'
            'CNCOSAELIAMIGHTY'
            'OOEPTNUCDCHARMSP'
            'COSIODAWFRQSKISO'
            'EZICILATIIUMECOE'
            'FEWCLYFEDCIAEARS'
            'ISAYSAEPATDSPLTY'];
       
    elseif strcmp(inputName,'input_04')

        puzzle = ['TWSUUNSELFISHLYA'
            'URESPIRATORSCQGS'
            'OHCECAULIFLOWERC'
            'RENIGMATICLIMBER'
            'TBETRAYINGTJMGNU'
            'BEIITPEKLIMONAET'
            'ELDDUMOGRACANTHI'
            'LBEINGMLEERZPSIN'
            'ERPPCDEFLOGSYMSI'
            'AEXUZTNUIISEPAOZ'
            'GFETSEIKCIWKERBE'
            'UIRSREGNUOYOIDAD'
            'ENITALICIZINGDRC'
            'REKAMWALLACROSSE'];

       
    elseif strcmp(inputName,'input_05')
        puzzle = ['RCPLEADERSIPUSHY'
            'EFEALVECPROOLZOK'
            'ILECGABETHGROZZC'
            'LLVINRYSSNADMMBI'
            'DAITAREVENTRROAR'
            'OBDSJASEGIPAOLTE'
            'GTIIMMUNMOVEDLTT'
            'IEOGAOWIDEATDULO'
            'MUMOHDDILLNESSES'
            'EQALWEUHLAITYKFE'
            'DCTFSMITUFLKSEIH'
            'WAITSSUWYLLFOWES'
            'FRCITSIVITALERLN'
            'SEIDLOCSOKSSEZDA'];

        
    elseif strcmp(inputName,'input_06')
        
        puzzle = ['YSSHEAVYSETQUIPS'
            'REDERYLTIRURNSPE'
            'AIRSUMALATECOMER'
            'ITAOTLUSHERTIIEU'
            'DEWOLFTLTEEITLKV'
            'ELPLUTILAFCGCISA'
            'MTUWVSRAPRTINHFR'
            'RBAZNEMWOAIDUCOG'
            'EUESAIBEERLTFTOO'
            'TSRGCUPTTJISSNHT'
            'NCUAHLCISSNMYAFO'
            'IFBGOGAHOHEIDICR'
            'YLFFITSWAEARRGNT'
            'YTIRALUGERRKFZSG'];

    elseif strcmp(inputName,'input_07')
        puzzle = ['MUCKIERNOISELESS'
            'AFFECTATIONSADEW'
            'SUWZHUFERCIRNNPM'
            'KMBHHALUTSAAOTAO'
            'RRHEAGLAMAHZIYYU'
            'YHOOALUVUICTTPSS'
            'SNONLEEKEFAQIEYE'
            'ABIMALASTDSMRWOR'
            'TFATBMULPNPRTRUF'
            'NITDUOIDVLIZUIRE'
            'ATRLTMINOERDNTSA'
            'FCYEXBCDDWEEWEEL'
            'YHCAECEIEEDNJRLT'
            'PYROODTUOSXYWSFY'];

    end

end