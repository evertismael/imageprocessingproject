% Owner: Evert P.C.
% Date: 30-04-18
% Read an image in the dataset and segment it in several images, containing
% only one letter in each image. The image is normalized to 64x64 pixels in
% a binary image.

function dataset = getDatasetLetters(numberDataset)
    
    fname = ['dataset/',num2str(numberDataset),'.jpg'];

    img = im2double(imread(fname));
    img = rgb2gray(img);
    img = im2bw(img,0.5);    
        
    abc = ['a';'b';'c';'d';'e';'f';'g';'h';'i';'j';'k';'l';'m';'n';'o';'p';'q';'r';'s';'t';'u';'v';'w';'x';'y';'z'];
    switch numberDataset
        case 1
            init_x = 295;
            step_x = 100;

            init_y = 495;
            step_y = 100;
            labels = [...
                'vytgnitapicnamex';'lfreelancingipqi';'thipurviewyirhah';'nndslionossetytb';
                'erehtipnedsrasea';'mremysesnihilist';'einaereeesmsdcte';'vhnimsonortheast';'ewreniaduioaslia';
                'icdedudbcgslaspe';'himjseokbellboyl';'cuvvniitfumulmtp';'arbalednacsreeek';'secidujerpxedyst'];
        case 2
            init_x = 290;
            step_x = 100;

            init_y = 462;
            step_y = 100;
            labels = [...
                'sasawhcszincking';'nitsusembxnheron';'irnsrillieotubdi';'paesiuldiscbpken';
                'gmmaggseertodawg';'ngehnnowtruamnli';'ilcsiieladrukeas';'kaairtaeotnemips';'rmleexhgsheppana';
                'aopgdetoaacrcorg';'lrurrrrneodymium';'tsigolonepktsarr';'sseteopjlsxemprd';'yhsurstoohnquark'];
        case 3
            init_x = 293;
            step_x = 100;

            init_y = 461;
            step_y = 100;
            labels = [...
                'qintersperseccnr';'nonstandardioeie';'rednibllepspmfkt';'mnsredutalsnpads';
                'doaaatopieujlvmk';'ehrfivpxggasiagc';'lpftgrituidgcguu';'eyakaslursihajnh';'essiiraiulemtuns';
                'hashyibdfaulejyi';'weaiztdobtsosusl';'nmssmeueaoirtbay';'iledracnarinject';'prusuttsssdegsks'];
        case 4
            init_x = 297;
            step_x = 100;

            init_y = 461;
            step_y = 100;
            labels = [...
                'sretuosrematract';'spuorgahaghastni';'hooplanetariauto';'persecutionrrnfr';
                'stsiegretlopsild';'emopusaisswmikua';'degnosgnisimssit';'icevpcuboopuytds';'rhnilderfrjelawa';
                'tacaolcsidavaoen';'snaolmosfogbngla';'eimelloweruaailc';'bcpivnprlsaskcee';'csseldneeyrevarb'
                ];
    end
    
    t_set = struct('label',{},'img',{},'feature',{});
    for i=1:length(abc)
        data = {};
        data.label = abc(i);
        data.img = [];
        data.feature = [];
        t_set(end+1) = data;
    end

    for row=1:14
        for col=1:16
            letter = img(...
                init_x + (row-1)*(step_x) : init_x + (row-1)*(step_x) + 63,...
                init_y + (col-1)*(step_y) : init_y + (col-1)*(step_y) + 63);
            for i=1:length(abc)
                if strcmp(t_set(i).label,labels(row,col))
                    t_set(i).img(end+1,:,:) = letter;
                end
            end

        end
    end

    
    % save the images.
    for i=1:length(abc)
       for j = 1:size(t_set(i).img,1)
           imgSave = reshape(t_set(i).img(j,:,:),64,64);
           imwrite(imgSave,['dataset/',num2str(numberDataset),'/',t_set(i).label,'_',num2str(j),'.jpg']);
       end
    end
    
    save(['dataset/',num2str(numberDataset),'/_data.mat'],'t_set');
    dataset = t_set;
end
    