% Owner: Evert P.C.
% Date: 01-05-18
% Receives a matrix, each element is another matrix containing the pixels
% of the image. The output is another matrix os the same dimensions of the
% input Matrix but every element is the class of the input element.

% Make sure to use the same parameters of the HOG.!!!!!

function matrixLabels = classifyImages2Letters(InputMatrix)
    % validating data. 
    Nrows = size(InputMatrix,1);
    Ncols = size(InputMatrix,2);
    imgRow = size(InputMatrix,3);
    imgCols = size(InputMatrix,4);
    validateInputs(Nrows, Ncols, imgRow, imgCols);
    
    classifier = EuclideanClassifier();     % initiates the classifier
    cellSize = [8 8]; %hog                   % hog cellsize
    
    matrixLabels = char(zeros(Nrows,Ncols));
    for r = 1:Nrows
        for c = 1:Ncols
            image = reshape(InputMatrix(r,c,:,:),64,64);
            %[features,~] = extractHOGFeatures(image,'CellSize',cellSize);
            [features] = myHog(image);
            matrixLabels(r,c) = char(classifier.classify(features));   % classify;
        end
    end  
end
% -------------------------------------------------------------------------
% ------------------Helper functions ----------------------------------
% -------------------------------------------------------------------------
function [] = validateInputs(Nrows, Ncols, imgRow, imgCols)
    if Nrows <= 0 || Ncols <= 0 || imgRow <= 0 || imgCols <= 0 
        error('Dimensions are negative!!!');
    end
    
    if imgRow ~= 64 || imgCols ~= 64 
        error('The images are not 64x64');
    end
    
end