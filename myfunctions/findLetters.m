function [output_final,borders] = findLetters(total_image) 
    [rows,columns] = size(total_image);
    total_image = imbinarize(total_image); %%convert to binary image with the threshold half of max value
    %% projection on the row(output is neg_edge_letter_row and pos_edge_letter_row) function used: equalization2dnew
    sensitivity = 40;%%sensitivity for decide the letters
    proj_row = sum(total_image,2);%%initinalization of the horizontal projection and vertical projection vector
    
    
    figure;
    plot(proj_row);
    title('row projection');
    xlim([0 3600]);
    ylim([1950 2500]);
    
    threshold = 2380;
    for i=1:length(proj_row)
        if proj_row(i)<threshold
            proj_row(i) = threshold;
        end
    end
    
    figure;
    plot(proj_row);
    title('row projection threshold');
    %     view([90 -90]);
    xlim([0 3600]);
    ylim([2360 2500]);
    
    
    j=0;                                                      %%j is the index for negative edge vector
    k=0;                                                      %%k is the index for positive edge vector
    for i=2:rows                                              %%i is the index for projection vector
        if proj_row(i)==threshold && proj_row(i-1)>threshold
                j=j+1;
                neg_edge(j) = i-1;                                 %%now neg_edge stores every negative edge index of the row
        elseif proj_row(i)>threshold&&proj_row(i-1)==threshold  %2471 is for when we got a Q which has a big tail
                k=k+1;
                pos_edge(k) = i;                                   %%now pos_edge stores every positive edge index of the row
        elseif proj_row(i)<=0.9*columns            
                proj_row(i)=columns;                               %%kick off the effect of the black line
        end

    end



    %%for this part,the local output (neg_edge_letter_row,pos_edge_letter_column)is the row index of only the letters
    neg_edge=neg_edge(2:end);
    pos_edge=pos_edge(2:end);                               %%kick off the pos&&neg edge of the title

    neg_edge_letter_row(1) = neg_edge(1);                       %%asume that after kicking off the title, the next one is always the first letter without any bias
    pos_edge_letter_row(1) = pos_edge(1);
    m=1;                                                    %%m is the index for letter negative edge
    n=1;                                                    %%n is the index for letter positive edge
    i=2;                                                    %%i is the index for negative edge
    j=2;                                                    %%j is the index for positive edge
    while i<(length(neg_edge)-2) && abs((neg_edge(i)-neg_edge(i-1))-(neg_edge(i+1)-neg_edge(i)))<sensitivity  %%if 3 neighbouring edges have similar step, then we put them as the start of a letter(cuz we've assumed that)
            m=m+1;
            neg_edge_letter_row(m) = neg_edge(i);
            i=i+1;
    end
    neg_edge_letter_row(m+1) = neg_edge(i);                      %%for the last value of neg_edge_letter

    while j<(length(pos_edge)-2) && abs((pos_edge(j)-pos_edge(j-1))-(pos_edge(j+1)-pos_edge(j)))<sensitivity  %%if 3 neighbouring edges have similar step, then we put them as the start of a letter(cuz we've assumed that)(hard way to differentialize the different freq.)
            n=n+1;
            pos_edge_letter_row(n) = pos_edge(j);
            j=j+1;
    end
    pos_edge_letter_row(n+1) = pos_edge(j);                      %%for the last value of neg_edge_letter

    %----------------------------------output passing---------
    upper_border = neg_edge_letter_row(1);
    lower_border = pos_edge_letter_row(end);
    
    
    row_distance_temp = zeros(1,length(pos_edge_letter_row)-1);
    for i = 1:length(pos_edge_letter_row)-1
        row_distance_temp(i) = pos_edge_letter_row(i+1) - pos_edge_letter_row(i);
    end
    
    row_distance = floor(sum(row_distance_temp)/(length(row_distance_temp)));
    
    %------------------------------------------------------------ this has
    %to be done before 'equalization2dbnew' before the real value is
    %destroyed
    
     [neg_edge_letter_row,pos_edge_letter_row] = equalization2dnew(neg_edge_letter_row,pos_edge_letter_row);  %%make the steps equal
     [neg_edge_letter_row,pos_edge_letter_row] = equalization2dnew(neg_edge_letter_row,pos_edge_letter_row);  %%make the steps equal


    if length(pos_edge_letter_row)~=length(neg_edge_letter_row)
        fprintf('error!plz change the value of sensitivity!');
    else
        fprintf('sensitivity correct!\n');
    end




    %% projection on the column 
    letter_part = total_image(neg_edge_letter_row(1):pos_edge_letter_row(length(pos_edge_letter_row)),:); %%take the middle part which contains only the letters

    proj_column = sum(letter_part,1)';                                    %%projection on the column

    [rows_letter_part,columns_letter_part] = size(letter_part); %%store the size of new middle part image for future usage

    figure;
    plot(proj_column);                                                      
    title('column projection');                                             %% plot the column projection
    ylim([850 1400]);
    
    index1 = 0;                                                             %%index1 is the index for neg_edge_letter_column
    index2 = 0;                                                             %%index2 is the index for pos_edge_letter column
    for i=2:columns
        if proj_column(i-1)==rows_letter_part && proj_column(i)<rows_letter_part
            index1=index1+1;
            neg_edge_letter_column(index1) = i-1;
        elseif proj_column(i) == rows_letter_part &&proj_column(i-1)<rows_letter_part
            index2=index2+1;
            pos_edge_letter_column(index2) = i;
        end

    end

    
    
    
    %----------------------------------output passing---------
    left_border = neg_edge_letter_column(1);
    right_border = pos_edge_letter_column(end);
    
    column_distance_temp = zeros(1,length(pos_edge_letter_column)-1);
    for i = 1:length(pos_edge_letter_column)-1
    column_distance_temp(i) = pos_edge_letter_column(i+1) - pos_edge_letter_column(i);
    end
    
    column_distance = floor(sum(column_distance_temp)/(length(column_distance_temp)));
    
    %---------------------------------this has
    %to be done before 'equalization2dbnew' before the real value is
    %destroyed
    
%     borders = [upper_border,lower_border,left_border,right_border,row_distance,column_distance]; %border output with the order 'up,down,left,right,row_distance,column_distance'
    borders = [upper_border,lower_border,row_distance,column_distance]; %border output with the order 'up,down,left,right,row_distance,column_distance'
    
    
    [neg_edge_letter_column,pos_edge_letter_column] = equalization2dnew(neg_edge_letter_column,pos_edge_letter_column);  %%make the steps equal


    %% prepare for the output
    gap_column = neg_edge_letter_column(2)-pos_edge_letter_column(1);%%the white gap between two different letter
    gap_added = floor(gap_column*0.18);

    neg_edge_letter_column = neg_edge_letter_column-gap_added;
    pos_edge_letter_column = pos_edge_letter_column+gap_added;

    len_1 = pos_edge_letter_row(1) - neg_edge_letter_row(1);      %%lenth of output 1st dim
    len_2 = pos_edge_letter_column(1) - neg_edge_letter_column(1);%%lenth of output 2rd dim
    len_3 = length(neg_edge_letter_row);                                 %%lenth of output 3th dim(chapter)
    len_4 = length(neg_edge_letter_column);                            %%lenth of output 4th dim(page)

    output = zeros(len_1+1,len_2+1,len_3,len_4);  %%pre-allocate

    %%comments:  the tail and the head of 'W' and 'M' has some 
    %%problem. So I can make the size of the first two dimension of the final 
    %%output a bit bigger. We can add 10% of the different of the step on both side.


    %%---------------------assign the values to the output edition2---------------------
    for index3 = 1:len_3

        for index4 = 1:len_4
            output(:,:,index3,index4) = total_image(neg_edge_letter_row(index3):pos_edge_letter_row(index3),neg_edge_letter_column(index4):pos_edge_letter_column(index4));
        end
    end

    
    %% make square output    function used: make_square
    len = max(len_1+1,len_2+1);
    output_final = zeros(len,len,len_3,len_4);  %%pre-allocate again
    
    for index3 = 1:len_3
        for index4 = 1:len_4
            output_final(:,:,index3,index4) = make_square(output(:,:,index3,index4));
        end
    end

end






