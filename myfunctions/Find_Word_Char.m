function [word, num] = Find_Word_Char(rows_image)

    proj_col = sum(rows_image(:,:,1));

    window_size = 160;
    window_b = (1/window_size)*ones(1,window_size);
    window_a = 1;
    proj_col_fil = filter(window_b,window_a,proj_col);        %low pass filter 
    proj_col_fil = movmean(proj_col_fil/50,140); %another low pass filter 
    
    water = [watershed(proj_col_fil),0,0];            %calculate the water sheds
    
    water_histo = zeros(max(water)+1,1);        %take the histogram
    for i=1:length(water)
       water_histo(water(i)+1) = water_histo(water(i)+1) + 1;  
    end
    
%     figure
%     plot(proj_col); hold on
%     plot(proj_col_fil*50)
%     
%     figure
%     plot(water)
    
% extract the word part
    n = length(water_histo);
    cnt = 0;
    stp = 1;
    while stp>0 && n>0
        if water_histo(n) > 300
            cnt = cnt + 1;
            col_no(cnt) = n-1;
        end 
        n = n - 1;
    end
    col_no = flip(col_no);
    num = length(col_no);
    
% find the start and end of each row
    for i=1:length(col_no)
        for j=2:length(water+2)-1
            if water(j)==col_no(i) && water(j-1)~=col_no(i)
                col_se(i,1) = j;
            elseif water(j)==col_no(i) && water(j+1)~=col_no(i)
                col_se(i,2) = j;
            end
        end
    end
   
% divid the image in words
%     col_ave = round(sum(col_se(:,2) - col_se(:,1))/length(col_no));
    col_ave = max(col_se(:,2) - col_se(:,1));
    if col_ave > round(size(rows_image,2)/4)
        col_ave = round(size(rows_image,2)/4);
    end
    col_diff = col_se(:,2) - col_se(:,1) - col_ave;
    col_shift_l = floor(col_diff/2);
    col_shift_r = ceil(col_diff/2);
    for i=1:length(col_no)
        if col_se(i,2) - col_se(i,1) > round(size(rows_image,2)/4)
            words(:,:,i) = rows_image(:,col_se(i,1):col_se(i,1)+col_ave,1);
        elseif col_se(i,2)-col_shift_r(i) > size(rows_image,2)
            words(:,:,i) = rows_image(:,end-col_ave:end,1);
        elseif col_se(i,1)+col_shift_l(i) < 1
            words(:,:,i) = rows_image(:,1:1+col_ave,1);
        else
            words(:,:,i) = rows_image(:,col_se(i,1)+col_shift_l(i):col_se(i,2)-col_shift_r(i),1);
        end
%         figure
%         imshow(words(:,:,i))
    end
    
    
    
    
    
% pick out each character from the words
    word = ones(64,64,length(col_no),16);

for i=1:length(col_no)
    proj_word = sum(words(:,:,i));
%     figure
%     plot(proj_word)
%     figure
%     imshow(words(:,:,i))

    %take the start and end of each letter
    cnt1 = 1;
    for j=2:col_ave-1
         if proj_word(j)==size(words,1) && proj_word(j+1)~=size(words,1)
            char_se_tmp(cnt1,1) = j;
         elseif proj_word(j+1)==size(words,1) && proj_word(j)~=size(words,1)
            char_se_tmp(cnt1,2) = j+1;
            cnt1 = cnt1 + 1;
         end
    end
    char_dif_tmp = char_se_tmp(:,2) - char_se_tmp(:,1);     %difference
    
    %separate the connected chars
    cnt2 = 1;
    for j=1:size(char_se_tmp,1)
        if char_dif_tmp(j) > size(rows_image,1)+3
%             top = sum(words(1:5,char_se_tmp(j,1):char_se_tmp(j,2),i));
%             bot = sum(words(30:35,char_se_tmp(j,1):char_se_tmp(j,2),i));
%             k = 2;
%             top_s = 2;
%             bot_s = 2;
% %             figure
% %             subplot(2,1,1)
% %             plot(top)
% %             subplot(2,1,2)
% %             plot(bot)
%             while top_s>0 || bot_s>0 && k<length(top)
%                 if top(k)==max(top) && top(k-1)~=max(top)
%                     top_l = k;
%                     top_s = top_s - 1;
%                 end
%                 if top(k)==max(top) && top(k+1)~=max(top)
%                     top_r = k;
%                     top_s = top_s - 1;
%                 end
%                 if bot(k)==max(bot) && bot(k-1)~=max(bot)
%                     bot_r = k;
%                     bot_s = bot_s - 1;
%                 end
%                 if bot(k)==max(bot) && bot(k+1)~=max(bot)
%                     bot_l = k;
%                     bot_s = bot_s - 1;
%                 end
%                 k = k + 1;
%             end
%             if top_s>0 || bot_s>0
                shift = floor(char_dif_tmp(j)/2);
%             else
%                 l = max(top_l,bot_l);
%                 r = min(top_r,bot_r);
%                 shift = min(l,r) + round(abs(r - l)/2);
%             end
            char_se(cnt2,1) = char_se_tmp(j,1);
            char_se(cnt2,2) = char_se_tmp(j,1) + shift;
            cnt2 = cnt2 + 1;
            char_se(cnt2,1) = char_se_tmp(j,1) + shift;
            char_se(cnt2,2) = char_se_tmp(j,2);
        else
            char_se(cnt2,:) = char_se_tmp(j,:);
        end
        cnt2 = cnt2 + 1;
    end
    char_dif = char_se(:,2) - char_se(:,1);     %difference
    
    %figure
    
    for j=1:size(char_se,1)
    char_shift = floor((size(rows_image,1)-char_dif(j))/2); %how much to shift
    char_tmp = ones(size(rows_image,1),size(rows_image,1)); %make 35x35 square
    char_one = words(:,char_se(j,1):char_se(j,2),i);          %pickout one letter 
    if size(char_one,2) > size(rows_image,1)
        char_tmp = imresize(char_one,[35 35]);  %if the char is wide then make it small
    else
        char_tmp(:,char_shift+1:char_shift+1+char_dif(j)) = char_one;  %put character in the square
    end
    char = imresize(char_tmp,[64 64]);
    subplot(1,size(char_se,1),j)
    %imshow(char)
    word(:,:,i,j) = char;
    end
    
    clear vars char_se_tmp char_dif_tmp char_se char_dif
end

end