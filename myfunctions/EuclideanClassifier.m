classdef EuclideanClassifier < handle
% 
properties
        centroids;
        labels;
   end

   methods
    % --------------- Constructor -------------------
    function obj = EuclideanClassifier()
        load('classifier.mat');
        obj.centroids = centroids;
        obj.labels = labels;
    end
    function class = classify(obj,inputVector)
        distance = Inf;
        for i=1:length(obj.labels)
            nd = sum((inputVector - obj.centroids(i,:)).^2);
            if nd <= distance
                class = obj.labels(i);
                distance = nd;
            end
        end
    end
   end
end