function [hogVector] = myHog(img)
    
    % ----- parameters.
    maskX = [-1 0 1];
    maskY = maskX';
    cellSize = 8;
    %
    [tR,tC] = size(img);
    if mod(tR,2)~=0 || mod(tC,2)~=0
        error('HOG - dimensions input image must be multiple of 2');
    end

    % step 1: compute gradient.
    sx = conv2(img,maskX,'same');
    sy = conv2(img,maskY,'same');

    s = sqrt(sx.^2 + sy.^2);
    theta = atan(sy./sx);
    theta(find(theta<0)) = theta(find(theta<0)) + pi;
    theta(find(isnan(theta))) = pi/2;
    theta = theta*(180/pi);

    % step 2: compute oriented histograms.
    vector_cells = zeros(tR/cellSize,tC/cellSize*9);
    for i_cx = 1:tR/cellSize
        for i_cy = 1:tC/cellSize
            s_cell = s((i_cx-1)*cellSize + 1:i_cx*cellSize,(i_cy-1)*cellSize + 1:i_cy*cellSize);
            theta_cell = theta((i_cx-1)*cellSize + 1:i_cx*cellSize,(i_cy-1)*cellSize + 1:i_cy*cellSize); 

            bin_votes = zeros(1,9);
            for r_cell = 1:cellSize
                for c_cell = 1:cellSize
                    angle = theta_cell(r_cell,c_cell);
                    magnitud = s_cell(r_cell,c_cell);
                    for i_bin = 1:9
                        bin_center = (i_bin-1)*20 + 10;
                        if abs(bin_center - angle)<20
                            bin_votes(i_bin) = bin_votes(i_bin) + (1-(abs(bin_center - angle)/20))*magnitud;
                        end
                    end
                end
            end
            vector_cells(i_cx,(i_cy-1)*9 + 1:(i_cy)*9) = bin_votes;
        end
    end

    % step 3: normalize.
    features = [];
    for i_cx = 1:tR/cellSize-1
        for i_cy = 1:tC/cellSize-1
            block = vector_cells(i_cx:i_cx+1,(i_cy-1)*9 + 1:(i_cy+1)*9);
            norm = sqrt(sum(sum(block.^2))+0.01);
            normBlock = block/norm;

            temp = normBlock';
            features = [features; temp(:)];
        end
    end
    
    hogVector = features';
    %plotHog(img,hogVector);
end