function [output1,output2] = equalization2dnew(input1, input2)

%%description: inputs are two vectors with the same length

        len = length(input1);
        
        distance = abs(input2-input1); %%distance vector
        
        step = mode(distance);  %%majority vote for the step
        
        edges = zeros(1,2*len); %%initialization for the combined output
        
        for i = 1:len
            if distance(i)==step                   %%step equal to the distance,do nothing
                edges(2*i) = input2(i);
                edges(2*i-1) = input1(i);   
            elseif distance(i)<step                %%step bigger than the distance, extend
                pad1 = floor((step-distance(i))/2);
                edges(2*i-1) = input1(i)-pad1;
                 if mod(step-distance(i),2)==0
                     edges(2*i) = input2(i)+pad1;  %%since we use floor,this extra 1 here make sure after extending on both sides it's still the same step 
                 else
                     edges(2*i) = input2(i)+pad1+1;
                 end
                      
            else
                pad2 = floor((distance(i)-step)/2);%%step smaller than the distance, shrink 
                edges(2*i-1) = input1(i)+pad2;
                if mod(distance(i)-step,2)==0
                    edges(2*i) = input2(i)-pad2;
                else
                    edges(2*i) = input2(i)-(pad2+1);%%since we use floor,this extra 1 here make sure after extending on both sides it's still the same step
                end
            end
        end
        
        output1 = edges(1:2:end);
        output2 = edges(2:2:end);    %%take the odd value as output1 and even value as output2

end