function [word] = Find_Word(inImage)

[rows_image] = Find_Word_Rows(inImage);    

counter = 0;
figure
for i=1:size(rows_image,3)    
    [word_tmp,num] = Find_Word_Char(rows_image(:,:,i));
    word(:,:,counter+1:counter+num,:) = word_tmp;
    counter = counter + num;
end

end