function [output] = make_square(input)

%---------------------function description---------------------------------
% the input is a 2D matrix, the output will make this matrix a with a same
% length and width by padding zeros to the smaller dimension

[row,column] = size(input);

    if row == column
    output = input;
        elseif row < column
    up = ones(floor((column-row)/2),column);
    down = ones(ceil((column-row)/2),column);
    output = [up;input;down];
        else 
    left = ones(row,floor((row-column)/2));
    right = ones(row,floor((row-column)/2));
    output = [left,input,right];
    end
end