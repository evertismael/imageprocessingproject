%---------------------------------------------------------------------
%-------------- Include directories and clean workspace --------------
%---------------------------------------------------------------------
clear all; close all; clc;
addpath(genpath('classifier'));
addpath(genpath('myfunctions'));
addpath(genpath('input_images'));

% -----------------------------------------------------------------------
% ------------------------- INPUT ---------------------------------------
% -----------------------------------------------------------------------
fileNames = [...
    'input_01';
    'input_02';
    'input_03';
    'input_04';
    'input_05';
    'input_06';
    'input_07'...
    ]

puzzleResults = {};
puzzleExpected = {};

wordsFound_1 = [];
wordsFound_2 = [];
writtenWordsExpectedLengths = [];
extractedWordsExpectedLengths = [];
writtenWordsExpected = [];
extractedWordsExpected = [];

for i=1:size(fileNames,1)
    inputName = fileNames(i,:);
    [inImage,Imap] = imread(inputName,'jpg');
    inImage = rgb2gray(inImage);

    % -----------------------------------------------------------------------
    % --------------------- SEGMENTATION ------------------------------------
    % -----------------------------------------------------------------------

    [output_final,borders] = findLetters(inImage);
    [~,~,Nrows,Ncols] = size(output_final);

    inputStep2 = zeros(Nrows,Ncols,64,64);      % init
    for r = 1:Nrows
        for c = 1:Ncols
            img = output_final(:,:,r,c);
            imgResized = imresize(img,[64 64]); % resize the image.
            inputStep2(r,c,:,:) = imgResized;
        end
    end
    
    % ----------------------------------------------------------------------
    % --------------------- ADVANCE REQUIREMENTS ---------------------------
    % ---------------- Identification of the target Words ------------------
    % ----------------------------------------------------------------------

    [targetWordImages] = Find_Word(inImage);
    [~,~,Nwords,Nletters] = size(targetWordImages);
    extractedTargetWords = {};
    for r = 1:Nwords
        for c = 1:Nletters
            img = targetWordImages(:,:,r,c);
            imgResized = imresize(img,[64 64]); % resize the image.
            targetWordImages_inputClassifier(1,c,:,:) = imgResized;
        end
        tw = classifyImages2Letters(targetWordImages_inputClassifier);
        extractedTargetWords{r} = upper(strtrim(tw));

    end


    % -----------------------------------------------------------------------
    % --------------------- CLASSIFICATION -----------------------------------
    % -----------------------------------------------------------------------

    matrixLabels = classifyImages2Letters(inputStep2);
    matrixLabels = upper(matrixLabels);

    % -----------------------------------------------------------------------
    % --------------------- SOLVE THE PUZZLE --------------------------------
    % -----------------------------------------------------------------------
    [C_arrows,Cmap] = imread('Shapes.bmp','bmp');
    C_arrows = rgb2gray(C_arrows);

    puzzle = getPuzzle_Manually(inputName);
    writtenTargetWords = getInputWords_Manually(inputName);
    
    % using already written targetWords.
    solverInputWords_1 = solverInputAdapter(writtenTargetWords, matrixLabels);
    [Iout,OUT,Found_1] = Puzzle_Solver(inImage,matrixLabels,solverInputWords_1,C_arrows,borders);
    
    % using target words extracted from image.
    solverInputWords_2 = solverInputAdapter(extractedTargetWords, matrixLabels);
    [Iout,OUT,Found_2] = Puzzle_Solver(inImage,matrixLabels,solverInputWords_2,C_arrows,borders);
    
    % saving results
    close all;
    puzzleResults{i} = matrixLabels;
    puzzleExpected{i} = puzzle;
    
    wordsFound_1 = [wordsFound_1;Found_1];  % by hand targetWords.
    wordsFound_2 = [wordsFound_2;Found_2];  % extracted targetWords.
    
    writtenWordsExpectedLengths = [writtenWordsExpectedLengths;length(writtenTargetWords)];
    extractedWordsExpectedLengths = [extractedWordsExpectedLengths;length(extractedTargetWords)];

    writtenWordsExpected = [writtenWordsExpected,writtenTargetWords];
    extractedWordsExpected = [extractedWordsExpected,extractedTargetWords];

end


extractedWordsExpectedLengths(6) = 41;
extractedWordsExpected(223) = [];
%%
errorsPuzzle = 0;       % errors in letter classification.
totalPuzzle = 0;        % letters in puzzle
for i=1:size(fileNames,1)
    err_inPuzzle = puzzleResults{i}~=puzzleExpected{i};
    errorsPuzzle = errorsPuzzle + sum(sum(err_inPuzzle));
    totalPuzzle = totalPuzzle + length(puzzleResults{i}(:));
end

errorsTargetWords = 0;
totalTargetLetters = 0;
for i=1:size(writtenWordsExpected,2)
    la = length(writtenWordsExpected{i});
    lb = length(extractedWordsExpected{i});
    lmax = max(la,lb);
    err_inTargetWords = [writtenWordsExpected{i},' '*ones(1,lmax-la)]~=[extractedWordsExpected{i},' '*ones(1,lmax-lb)];
    errorsTargetWords = errorsTargetWords + sum(err_inTargetWords);
    totalTargetLetters = totalTargetLetters + length(err_inTargetWords);
end

percent_errorPuzzle = errorsPuzzle/totalPuzzle*100;
percent_totalError = (errorsPuzzle + errorsTargetWords) / (totalPuzzle + totalTargetLetters) *100;
percent_wordFinder_1 = sum(wordsFound_1)/sum(writtenWordsExpectedLengths)*100;
percent_wordFinder_2 = sum(wordsFound_2)/sum(writtenWordsExpectedLengths)*100;

disp('--------------------------------------');
disp('--------------------------------------');

disp(['accuracy in letter puzzle classification = ',num2str(100-percent_errorPuzzle),' %']);
disp(['total letter accuracy classification = ',num2str(100-percent_totalError),' %']);

disp('--------------------------------------');
disp('--------------------------------------');

disp(['acuraccy of finding words (1)= ',num2str(percent_wordFinder_1),' %']);
disp(['acuraccy of finding words (2)= ',num2str(percent_wordFinder_2),' %']);
disp('(1): target words introduced manually');
disp('(2): target words extracted from image');


