%---------------------------------------------------------------------
%-------------- Include directories and clean workspace --------------
%---------------------------------------------------------------------
clear all; close all; clc;
addpath(genpath('classifier'));
addpath(genpath('myfunctions'));
addpath(genpath('input_images'));

% -----------------------------------------------------------------------
% ------------------------- INPUT ---------------------------------------
% -----------------------------------------------------------------------
inputName = 'input_06';
[inImage,Imap] = imread(inputName,'jpg');
inImage = rgb2gray(inImage);

% -----------------------------------------------------------------------
% --------------------- SEGMENTATION ------------------------------------
% -----------------------------------------------------------------------
% for i = 1:10

[output_final,borders] = findLetters(inImage);
[~,~,Nrows,Ncols] = size(output_final);

inputStep2 = zeros(Nrows,Ncols,64,64);      % init
for r = 1:Nrows
    for c = 1:Ncols
        img = output_final(:,:,r,c);
        imgResized = imresize(img,[64 64]); % resize the image.
        inputStep2(r,c,:,:) = imgResized;
    end
end

% ----------------------------------------------------------------------
% --------------------- ADVANCE REQUIREMENTS ---------------------------
% ---------------- Identification of the target Words ------------------
% ----------------------------------------------------------------------

[targetWordImages] = Find_Word(inImage);
[~,~,Nwords,Nletters] = size(targetWordImages);
extractedTargetWords = {};
for r = 1:Nwords
    for c = 1:Nletters
        img = targetWordImages(:,:,r,c);
        imgResized = imresize(img,[64 64]); % resize the image.
        targetWordImages_inputClassifier(1,c,:,:) = imgResized;
    end
    tw = classifyImages2Letters(targetWordImages_inputClassifier);
    extractedTargetWords{r} = upper(strtrim(tw));
    
end

% -----------------------------------------------------------------------
% --------------------- CLASSIFICATION -----------------------------------
% -----------------------------------------------------------------------

matrixLabels = classifyImages2Letters(inputStep2);
matrixLabels = upper(matrixLabels);

% -----------------------------------------------------------------------
% --------------------- SOLVE THE PUZZLE --------------------------------
% -----------------------------------------------------------------------
[C_arrows,Cmap] = imread('Shapes.bmp','bmp');
C_arrows = rgb2gray(C_arrows);

puzzle = getPuzzle_Manually(inputName);
writtenTargetWords = getInputWords_Manually(inputName);

% with target words intruduced by hand.
solverInputWords_1 = solverInputAdapter(writtenTargetWords, matrixLabels);
[Iout,OUT] = Puzzle_Solver(inImage,matrixLabels,solverInputWords_1,C_arrows,borders);

% with target words extracted from image.
figure;
solverInputWords_2 = solverInputAdapter(extractedTargetWords, matrixLabels);
[Iout,OUT] = Puzzle_Solver(inImage,matrixLabels,solverInputWords_2,C_arrows,borders);
