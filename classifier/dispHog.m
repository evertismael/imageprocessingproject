% display hog.
% Owner: Evert P.C.
% Date: 30-04-18
% Generate the values that the clasifier needs to evaluate if a given input
% image belongs to a specific class. This script must be executed under the
% directory: 'Project/classifier' otherwise the it can do unexpected work.
%---------------------------------------------------------------------
%-------------- Include directories and clean workspace --------------
%---------------------------------------------------------------------
clear all; close all; clc;
addpath(genpath('../myfunctions'));
% ------------ Params --------------------
cellSize = [8 8]; %hog

letter = im2bw(im2double(imread('dataset/1/b_1.jpg')),0.5);
[featureVector,dispHog] = extractHOGFeatures(letter,'CellSize',cellSize);
figure;
subplot(1,2,1)
imshow(letter);
title('Input Letter');
subplot(1,2,2);
plot(dispHog);
title('HOG representation');