% Owner: Evert P.C.
% Date: 30-04-18
% Generate the values that the clasifier needs to evaluate if a given input
% image belongs to a specific class. This script must be executed under the
% directory: 'Project/classifier' otherwise the it can do unexpected work.
%---------------------------------------------------------------------
%-------------- Include directories and clean workspace --------------
%---------------------------------------------------------------------
clear all; close all; clc;
addpath(genpath('../myfunctions'));
% ------------ Params --------------------
cellSize = [8 8]; %hog
% -----------------------------------------------------------------------
% ---- Generate the database - extract images letters from puzzle ----
data = [];
data = [data; getDatasetLetters(1)];
data = [data; getDatasetLetters(2)];
data = [data; getDatasetLetters(3)];
data = [data; getDatasetLetters(4)];

% ----- Merge the data ---------
dataset = data(1,:);
for i=1:size(data,1)
    if i > 1
        for j=1:size(dataset,2)
            dataset(1,j).img = [dataset(1,j).img(:,:,:);data(i,j).img(:,:,:)];
        end
    end
end
dataset(1,j+1) = dataset(1,j);
dataset(1,j+1).label = '';
dataset(1,j+1).img = ones(1,64,64);

% ------ extracting features of every element in the dataset ------
for i=1:size(data,2)+1
   for j = 1:size(dataset(i).img,1)
       letter = reshape(dataset(i).img(j,:,:),64,64);
       %[featureVector,~] = extractHOGFeatures(letter,'CellSize',cellSize);
       [featureVector] = myHog(letter);
       dataset(i).feature(end+1,:) = featureVector;
   end
end

% -------------------------------------------------------------------------
% -------------------------- Build Clasifier ------------------------------
% -------------------------------------------------------------------------
% the simplest version is a euclidean distance clasifier: for that we
% compute the centroids of every class as the average of the vectors of the
% same class.

centroids = zeros(size(data,2)+1,size(dataset(1).feature,2));
for i=1:size(data,2)+1
   features = dataset(i).feature;
   if size(features,1)>1
      centroids(i,:) = sum(features) / size(dataset(i).feature,1);
   else
      centroids(i,:) = features;
   end
end

% ---------- saving the centroids and labels for future use ---------------
labels = ['a';'b';'c';'d';'e';'f';'g';'h';'i';'j';'k';'l';'m';'n';
    'o';'p';'q';'r';'s';'t';'u';'v';'w';'x';'y';'z';' '];
save('classifier.mat','labels','centroids');